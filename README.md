# NumGen

>Generates  random number from a file

## Requirements

 * python3

## Install
 ```
 $ pip install pyfiglet
 ```
## ScreenShots

<img src="https://gitlab.com/AbiramK/numgen/raw/master/assets/screenshot.png" width="600">

## License

[GNU][License]

[LICENSE]: https://www.gnu.org/licenses/gpl-3.0.en.html