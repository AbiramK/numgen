import re
from random import random
from pyfiglet import Figlet
import time
import sys

f = Figlet(font='slant')
print(f.renderText('Random No Generator'))
print("="*72)
print("Description:")
print("\t Generates a random number from the numbers in a file specified.")
print("="*72)
print("\n\n")
print("\n1.Select File")
print("\n2.Generate Number")
print("\n3.Exit")
while True:
	choice = int(input("\nEnter your Choice:"))

	if(choice == 1):
		filename = input("\nEnter the name of file(eg:abc.txt):")
		try:
			file = open(filename,"r")
		except:
			print("File not found")

	elif(choice == 2):
		try:
			numbers = []
			for data in open(filename):   
				lines = data.split(" ")
				for words in lines:
					numbers.append(re.findall('[\d]+',words))
			numlist = [j for i in numbers for j in i]
			rand = int(random()*10%len(numlist))
			print(numlist[rand])
		except:
			print("Select the file first..")

	elif(choice == 3):
		ex = "Exciting...."
		for l in ex:
			sys.stdout.write(l)
			sys.stdout.flush()
			time.sleep(0.1)
		break